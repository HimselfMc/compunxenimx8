﻿For building Compulab Mini iMX8 kernel merged with Xenomai:

1) Configure the build directories
Choose a working directory and create subdirectories for the Xenomai ARM64 Kernel source, the Compulab Kernel source, the Compulab Kernel patches and a build directory:
cd <WorkDir>
mkdir Xenomai
mkdir Compulab
mkdir CompulabPatches
mkdir BuildKernelMx8 

2) Obtaining the sources
Clone the GIT repositories for the Xenomai  ARM64 Kernel sources, the Compulab Kernel source, the Compulab Kernel patches and checkout the correct branches:
cd Xenomai
git clone https://gitlab.denx.de/Xenomai/ipipe-arm64.git
git checkout ipipe-core-4.14.78-arm64-2 
cd CompulabPatches
git clone -b master https://github.com/compulab-yokneam/meta-bsp-imx8mm.git 
cd ../Compulab
git clone https://source.codeaurora.org/external/imx/linux-imx.git
git checkout rel_imx_4.14.78_1.0.0_ga 

3) Patching
Create a branch of the Compulab source and patch them with the Compulab patches, (the reject switch will keep all the failed patches):

git checkout -b CompulabXenomia
git apply --reject <WorkDir>/CompulabPatches/meta-bsp-imx8mm/recipes-kernel/linux/compulab/imx8mm/*.patch 
EXAMPLE OUTPUT:
====================================================================================
git apply --reject ../../Imx8Mm/meta-bsp-imx8mm/recipes-kernel/linux/compulab/imx8mm/*.patch
Checking patch arch/arm64/boot/dts/Makefile...
Checking patch arch/arm64/boot/dts/compulab/Makefile...
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Checking patch arch/arm64/configs/ucm-imx8m-mini_defconfig...
Applied patch arch/arm64/boot/dts/Makefile cleanly.
Applied patch arch/arm64/boot/dts/compulab/Makefile cleanly.
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Applied patch arch/arm64/configs/ucm-imx8m-mini_defconfig cleanly.
Checking patch drivers/media/platform/mxc/capture/ov5640_mipi_v2.c...
Hunk #1 succeeded at 504 (offset -32 lines).
Applied patch drivers/media/platform/mxc/capture/ov5640_mipi_v2.c cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/boot/dts/compulab/sb-ucm-imx8-rev2.dtsi...
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Applied patch arch/arm64/boot/dts/compulab/sb-ucm-imx8-rev2.dtsi cleanly.
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/boot/dts/compulab/sb-ucm-imx8-rev2.dtsi...
Applied patch arch/arm64/boot/dts/compulab/sb-ucm-imx8-rev2.dtsi cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Checking patch arch/arm64/configs/ucm-imx8m-mini_defconfig...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Applied patch arch/arm64/configs/ucm-imx8m-mini_defconfig cleanly.
Checking patch arch/arm64/boot/dts/compulab/sb-ucm-imx8-rev2.dtsi...
Applied patch arch/arm64/boot/dts/compulab/sb-ucm-imx8-rev2.dtsi cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Checking patch arch/arm64/configs/ucm-imx8m-mini_defconfig...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Applied patch arch/arm64/configs/ucm-imx8m-mini_defconfig cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Checking patch arch/arm64/configs/ucm-imx8m-mini_defconfig...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Applied patch arch/arm64/configs/ucm-imx8m-mini_defconfig cleanly.
Checking patch arch/arm64/boot/dts/compulab/Makefile...
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini-thermal.dts...
Applied patch arch/arm64/boot/dts/compulab/Makefile cleanly.
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini-thermal.dts cleanly.
Checking patch drivers/gpu/drm/bridge/Kconfig...
Checking patch drivers/gpu/drm/bridge/Makefile...
Checking patch drivers/gpu/drm/bridge/sn65dsi83/Kconfig...
Checking patch drivers/gpu/drm/bridge/sn65dsi83/Makefile...
Checking patch drivers/gpu/drm/bridge/sn65dsi83/sn65dsi83_brg.c...
Checking patch drivers/gpu/drm/bridge/sn65dsi83/sn65dsi83_brg.h...
Checking patch drivers/gpu/drm/bridge/sn65dsi83/sn65dsi83_drv.c...
Checking patch drivers/gpu/drm/bridge/sn65dsi83/sn65dsi83_timing.h...
Applied patch drivers/gpu/drm/bridge/Kconfig cleanly.
Applied patch drivers/gpu/drm/bridge/Makefile cleanly.
Applied patch drivers/gpu/drm/bridge/sn65dsi83/Kconfig cleanly.
Applied patch drivers/gpu/drm/bridge/sn65dsi83/Makefile cleanly.
Applied patch drivers/gpu/drm/bridge/sn65dsi83/sn65dsi83_brg.c cleanly.
Applied patch drivers/gpu/drm/bridge/sn65dsi83/sn65dsi83_brg.h cleanly.
Applied patch drivers/gpu/drm/bridge/sn65dsi83/sn65dsi83_drv.c cleanly.
Applied patch drivers/gpu/drm/bridge/sn65dsi83/sn65dsi83_timing.h cleanly.
Checking patch arch/arm64/boot/dts/compulab/Makefile...
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini-lvds.dts...
Applied patch arch/arm64/boot/dts/compulab/Makefile cleanly.
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini-lvds.dts cleanly.
Checking patch drivers/gpu/drm/panel/Kconfig...
Checking patch drivers/gpu/drm/panel/Makefile...
Checking patch drivers/gpu/drm/panel/panel-startek-ili9881c.c...
Applied patch drivers/gpu/drm/panel/Kconfig cleanly.
Applied patch drivers/gpu/drm/panel/Makefile cleanly.
Applied patch drivers/gpu/drm/panel/panel-startek-ili9881c.c cleanly.
Checking patch arch/arm64/boot/dts/compulab/Makefile...
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini-dsi2.dts...
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Applied patch arch/arm64/boot/dts/compulab/Makefile cleanly.
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini-dsi2.dts cleanly.
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/configs/ucm-imx8m-mini_defconfig...
Applied patch arch/arm64/configs/ucm-imx8m-mini_defconfig cleanly.
Checking patch arch/arm64/configs/ucm-imx8m-mini_defconfig...
Applied patch arch/arm64/configs/ucm-imx8m-mini_defconfig cleanly.
Checking patch drivers/gpu/drm/panel/panel-startek-ili9881c.c...
Applied patch drivers/gpu/drm/panel/panel-startek-ili9881c.c cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/configs/ucm-imx8m-mini_defconfig...
Applied patch arch/arm64/configs/ucm-imx8m-mini_defconfig cleanly.
Checking patch arch/arm64/configs/ucm-imx8m-mini_defconfig...
Applied patch arch/arm64/configs/ucm-imx8m-mini_defconfig cleanly.
Checking patch arch/arm64/configs/ucm-imx8m-mini_defconfig...
Hunk #1 succeeded at 445 (offset 2 lines).
Applied patch arch/arm64/configs/ucm-imx8m-mini_defconfig cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Hunk #1 succeeded at 775 (offset 139 lines).
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Hunk #1 succeeded at 864 (offset 22 lines).
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini.dts cleanly.
Checking patch arch/arm64/configs/ucm-imx8m-mini_defconfig...
Applied patch arch/arm64/configs/ucm-imx8m-mini_defconfig cleanly.
Checking patch arch/arm64/boot/dts/compulab/Makefile...
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini-m4.dts...
Applied patch arch/arm64/boot/dts/compulab/Makefile cleanly.
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini-m4.dts cleanly.
Checking patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini-lvds.dts...
Applied patch arch/arm64/boot/dts/compulab/ucm-imx8m-mini-lvds.dts cleanly.
Checking patch drivers/mxc/gpu-viv/hal/os/linux/kernel/platform/freescale/gc_hal_kernel_platform_imx.c...
error: while searching for:
if (of_find_compatible_node(NULL, NULL, "fsl,imx8mm-gpu"))
{ 
Platform->flagBits |= gcvPLATFORM_FLAG_IMX_MM; 
}
return gcvSTATUS_OK; 
} 
error: patch failed: drivers/mxc/gpu-viv/hal/os/linux/kernel/platform/freescale/gc_hal_kernel_platform_imx.c:1521
Applying patch drivers/mxc/gpu-viv/hal/os/linux/kernel/platform/freescale/gc_hal_kernel_platform_imx.c with 1 reject...
Rejected hunk #1. 

Patch 33 fails as there is an update to the file that executes in the same way, but the last 3 patches do not get patched, (Don’t know how to force a continue on a sequenced set of patches), so need to be done individually:
git apply <WorkDir>/CompulabPatches/meta-bsp-imx8mm/recipes-kernel/linux/compulab/imx8mm/0034-Move-usb-eth-dongle-asix-88179-driver-into-kernel.patch
git apply <WorkDir>/CompulabPatches/meta-bsp-imx8mm/recipes-kernel/linux/compulab/imx8mm/0035-Enable-usb-to-serial-converters.patch
git apply <WorkDir>/CompulabPatches/meta-bsp-imx8mm/recipes-kernel/linux/compulab/imx8mm/0036-Enable-iwlwifi-Intel-wireless-mPCI-card.patch 

4) Merging
Change the remote for the GIT to point to the Xenomia source and merge without commiting so can easily see and examine the changes:
git remote set-url origin file:///<WorkDir/Xenomia/ipipe-arm64/ 
git merge origin --no-ff --no-commit --allow-unrelated-histories 

Fix any conflicts, (Had a simple problem with the machine configuration file), and commit the changes:
git add .
git commit . -m "Xenomia merged" 

5) Building
Setup the build configuration switches:
export MACHINE=ucm-imx8m-mini
export CROSS_COMPILE=aarch64-linux-gnu-
export ARCH=arm64 

Configure the Kernel with the Xenomia ipipe support:
make O=../BuildKernelMx8/ ${MACHINE}_defconfig 
make O=../BuildKernelMx8/ menuconfig 

Recommendations for the Xenomia configuration taken from:
http://events19.linuxfoundation.org/wp-content/uploads/2018/07/OSS-JAPAN-2019-XENOMAI-BASED-REAL-TIME-MODEL-1.pdf 

ENABLE:
Symbol: IPIPE [=y] 
│ Type : boolean 
│ Prompt: Interrupt pipeline
│ Location: 
│ -> Kernel Features 
DISABLE:
Symbol: CPU_IDLE [=n] 
│ Type : boolean 
│ Prompt: CPU idle PM support
│ Location: 
│ -> CPU Power Management
│ -> CPU Idle 
Symbol: CPU_FREQ [=n] 
│ Type : boolean 
│ Prompt: CPU Frequency scaling
│ Location: 
│ -> CPU Power Management 
│ -> CPU Frequency scaling 
Symbol: KGDB [=n] 
│ Type : boolean 
│ Prompt: KGDB: kernel debugger
│ Location: 
│ -> Kernel hacking 

6) Build the Kernel sources:
make O=../BuildKernelMx8/ 

